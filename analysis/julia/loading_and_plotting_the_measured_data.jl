using CSV, DataFrames, DataFramesMeta, Dates, TimeSeries

# Choosing the experiment (experiment1 through experiment11):
experiment = "experiment4"

filenames = String[]
carnumbers = String[]
for (root, dirs, files) in walkdir(joinpath("../../experiments",experiment))
   for file in files
      if endswith(file, ".csv")
         push!(filenames,joinpath(root, file)) # path to files
         push!(carnumbers,file[4:5])
      end
   end
end

h = TimeArray[]

ϵs = 0.4       # threshold for the standard deviation of the speed measurement
ϵd = 0.05      # threshold for the standard deviation of the distance measurement

for filename in filenames
   f = CSV.read(filename,delim='\t') # creates a full DataFrame
   g = f[:,[Symbol("# datetime"), :gSpeed, :relPosLength, :sAcc, :accLength, :relPosValid]] # picks just a few columns
   DataFrames.rename!(g,[:datetime, :speed, :distance, :speedStDev, :distanceStDev, :distanceValid])     # renames the columns, in particular the clumsy first one
   g[!,:distance] = convert(Array{Union{Missing,Float64},1},g[!,:distance]) # enables changning some entries to the type MISSING later on
   g[!,:speed] = convert(Array{Union{Missing,Float64},1},g[!,:speed]) # enables changning some entries to the type MISSING later on
   t = Time[]           # seems like the datetime column must be parsed manually when it contains microseconds :-(
   for s in g[!,:datetime]
      hour = parse(Int,s[12:13])
      minute = parse(Int,s[15:16])
      second = parse(Int,s[18:19])
      ms = length(s) > 20 ? parse(Int,s[21:23]) : 0
      μs = length(s) > 23 ? parse(Int,s[24:26]) : 0
      #push!(t,Time(hour,minute,second,ms,μs))
      push!(t,Time(hour,minute,second,ms))
   end
   g[!,:datetime] = t   # if only this could be done without the loop above
   DataFrames.rename!(g,[:time, :speed, :distance, :speedStDev, :distanceStDev, :distanceValid])     # renaming the first column becase now it only holds the time
   g[!,:distanceValid] = (g[!,:distanceValid].=="True") # converting the column from strings to boolean
   g = @byrow! g if :distance == 0.0; :distance = missing end
   g = @byrow! g if !(:distanceValid) || :distanceStDev > ϵd; :distance = missing end
   g = @byrow! g if !(:distanceValid) || :speedStDev > ϵs; :speed = missing end
   ta = TimeArray(g,timestamp = :time)
   push!(h,ta)
end

# Defining the time window to be plotted

t0 = Time(12,15,10)   # for Experiment 11
t1 = Time(12,16,30)

t0 = Time(12,03,00)   # For Experiment 10
t1 = Time(12,09,30)

t0 = Time(12,07,30)   # For Experiment 10, zooming in into the dip region
t1 = Time(12,08,40)

t0 = Time(11,21,00)   # For Experiment 5, zooming in into the dip region
t1 = Time(11,22,30)

#h = from.(h,t0        # Comment out if you want to see the whole data set
#h = to.(h,t1)         # and not just a window

using Plots, PlotThemes
pyplot()
theme(:juno)

l = @layout [a; b]

p1 = plot(h[end][:speed],ylabel="Speed [m/s]",xlabel="t [s]",linewidth=3, label="Leader")
for i = 1:(length(h)-1)
   plot!(h[i][:speed],label="Car "*carnumbers[i])
end

p2 = plot(h[end][:distance],ylabel="Distance [m]",xlabel="t [s]",linewidth=3, label="")
for i = 1:(length(h)-1)
   plot!(h[i][:distance],label="")
end

plot(p1, p2, layout = l)
