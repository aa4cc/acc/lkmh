%% Importing, preprocessing and analyzing the data from uBlox GNSS receiver
% folder = "D:/LAB/AuticlaLetiste/samekacc/data/ublox/";
% folder = "D:\LAB\AuticlaLetiste\samekacc\analysis\matlab\TestDataFolder\";
folder = "../../experiments/experiment10/";


discard_invalid = false;   % discard unvalid data according to accLength value.
threshold = 0.05 ;          % discard threshold according to accLength value.

leader_file = dir(folder+"Leader*.csv");
follower_files = dir(folder+"Car*.csv");

%% Importing the data from CSV files
% The data is imported as tables

leader = importLeaderFile(folder+leader_file.name);
clear follower
clear names
for i = 1:numel(follower_files)
    disp(follower_files(i).name(1:5))
    names{i} = follower_files(i).name(1:5);
    follower{i} = importCarFile(folder+follower_files(i).name);
end

%% Converting tables to timetables
% Since the first column contains date and time, the timetable format seems
% appropriate.

leader = table2timetable(leader);

for i = 1:numel(follower)
    follower{i} = table2timetable(follower{i});
end

%% Get just the relevant columns from the tables
% The two time tables contain a wealth of columns but here we will only use
% one and two of them, respectively. 

leader = leader(:,'gSpeed');
for i = 1:numel(follower)
    follower{i} = follower{i}(:,{'gSpeed','relPosLength','accLength','relPosValid'});
end

%% discard invalid data
if discard_invalid
    for i = 1:numel(follower)
        map = (follower{i}.accLength>threshold) | (double(follower{i}.relPosValid)==1) ;
        follower{i}.gSpeed(map)= NaN;
        follower{i}.relPosLength(map)= NaN;
    end
end

if true
    for i = 1:numel(follower)
        follower{i}.relPosLength(follower{i}.relPosLength == 0)= NaN;
    end 
end

% %% artificially delay data - only for test purposes, don't forget to comment out !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% for i = 1:numel(follower)
% follower{i}.datetime = follower{i}.datetime + (i-1)*(1/24/60/60*5);
% end



%% Plotting the original data
% Let's explore the data visually first.

% set new color order so we have enough unique colors in plot
co = distinguishable_colors(numel(follower)+1);
set(groot,'defaultAxesColorOrder',co)

leg = strings(numel(follower)+1,1);
leg(1) = "Leader";
for i = 1:numel(follower)
    leg(i+1) = names{i};
end
close all
figure('units','normalized','outerposition',[0 0 1 1]);
ax1 =subplot(2,1,1);
plot(leader.datetime, leader.gSpeed*3.6,'LineWidth',3)
hold on
for i = 1:numel(follower)
plot(follower{i}.datetime, follower{i}.gSpeed*3.6,'LineWidth',1.5)
end
legend(leg)
ylabel('Speed (km/h)');
hold off
ax2 =subplot(2,1,2);
ax2.ColorOrderIndex = 2;
hold on
for i = 1:numel(follower)
plot(follower{i}.datetime, follower{i}.relPosLength,'LineWidth',1.5)
end
xlabel('Time (hh:mm:ss)')
ylabel('Distance (m)');
linkaxes([ax1,ax2],'x');
hold off

% print -dsvg experiment_snapshot.svg
% print -dpng -r300 experiment_snapshot.png
% print -depsc2 experiment_snapshot.eps
% !epstopdf experiment_snapshot.eps

