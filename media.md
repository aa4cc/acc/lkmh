# Pokrytí akce v médiích

- Rozhovor vedený moderátorem Ondřejem Topinkou s pořadatelem akce Zdeňkem Hurákem ve Studiu 6 ČT v pátek v 6:52, [[Odkaz]](https://www.ceskatelevize.cz/porady/1096902795-studio-6/219411010101206/).  
- Reportáž České televize z místa konání akce zařazená do hlavních večerních zpráv 6.12.2019, [[Odkaz]](https://ct24.ceskatelevize.cz/domaci/2997845-stroj-jako-clovek-i-adaptivni-tempomaty-umi-vytvorit-zbytecnou-kolonu) (obsahuje video).
- Reportáž Vojtěcha Kovala z Radiožurnálu Českého rozhlasu z místa konání akce, [[Odkaz]](https://radiozurnal.rozhlas.cz/pokud-jde-o-jizdu-v-kolone-trapi-stroje-podobne-problemy-jako-lidi-zjistili-8120406) (obsahuje audio i video).
- Článek Radka Pecáka "Může adaptivní tempomat zabránit zácpám? Vědci z ČVUT to ověřili při experimentu" na webu Automix.denik.cz, [[Odkaz]](https://automix.denik.cz/zivot-ridice/muze-adaptivni-tempomat-zabranit-zacpam-vedci-z-cvut-to-overili-pri-experimentu-20191208.html).
- Reportáž pořadu Garáž ze Seznam.cz
- Článek Martina Freie "Plynulejší provoz? Ani omylem. Autonomní auta na sebe reagují zmateně jako lidi" na webu Aktualne.cz, [[Odkaz]](https://zpravy.aktualne.cz/ekonomika/auto/plynulejsi-provoz-ani-omylem-autonomni-auta-na-sebe-reaguji/r~56c4fe741c3211ea84260cc47ab5f122/).
- Článek Luďka Vokáče "Adaptivní tempomaty mohou zhoršovat dopravní zácpy, zjistili na ČVUT" na webu idnes.cz, [[Odkaz]](https://www.idnes.cz/auto/zpravodajstvi/adaptivni-tempomaty-dopravni-zacpy-test-cvut.A200105_183507_automoto_vok).
- Tisková zpráva před začátkem experimentu "ČVUT pořádá unikátní veřejný experiment a zve ke spolupráci majitele aut s adaptivním tempomatem" na webu Otechnice.cz, [[Odkaz]](https://otechnice.cz/cvut-porada-unikatni-verejny-experiment-a-zve-ke-spolupraci-majitele-aut-s-adaptivnim-tempomatem/?utm_source=www.seznam.cz&utm_medium=sekce-z-internetu).
