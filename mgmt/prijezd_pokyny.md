# Pokyny pro příjezd účastníků na experimentální ověřování adaptivních tempomatů na letišti Mnichovo Hradiště v pátek 6.12.2019

- Příjezd očekáván 9:55 až 10:20
- Přijíždět se bude od obce Březina (exit 63 na D10), z Březiny po silnici 610 na Hoškovice. Krátce za Březinou odbočka doprava na letiště, závora (https://mapy.cz/s/lohurezofo, 50.5410875N, 15.0225711E).
![](mapa_prijezd.png)
- Od závory v doprovodu. 
