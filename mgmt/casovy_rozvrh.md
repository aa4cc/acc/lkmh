# Rozvrh a instrukce pro celou akci ověřování chování adaptivních tempomatů na Letišti Mnichovo Hradiště 6.12.2019

## 9:55-10:20 Příjezd účastníků

## 10:30 Přivítání a poučení
- Přivítání, představení organizátorů a partnerů
- Vysvětlení časového rozvrhu akce
- Vysvětlení motivace, cíle a náplně experimentu
- [Poučení o pohybu na letišti](LKMH-ID-17-02-19-Letistni-rad.pdf): modrá zóna, ...,
- Poučení o bezpečnosti při experimentu, podpis prohlášení účastníka
- Očíslování aut a přidělení spolujezdců do aut

## 11:00 první společné jízdy

Začíná se z prostoru start/cíl.

### 1. jízda (bez zapnutých adaptivních tempomatů)

- Lídr pojede na rovince 60 km/h a na rovince nebude zrychlovat ani brzdit
- Ostatní za ním v bezpečných odstupech (alespoň 2s)
- Zatáčky projíždět mírně (<= 50 km/h)

### 2. jízda (se zapnutými ACC, bez zrychlování lídra)

- Lídr pojede 60 km/h a nebude zrychlovat ani brzdit
- ACC zapnuty až na rovince (po výjezdu ze zatáčky), ze zatáčky vyjíždět ne rychleji než 50 km/h a dát předchozímu vozidlu dostatečný "náskok" před zapnutím vlastního adaptivního tempomatu  
- ACC nastaveny na 70 km/h (aby nedošlo k rozpojení), zvolen nejdelší odstup
- Všímat si, kde přibližně dojde k ustálení (rychlosti a vzdálenosti) u několika posledních vozů a tedy celé kolony
- Na konci rovinky (před apronem) ACC vypnuty
- Na zpáteční cestě na rovince (od apronu do prostoru start/cíl) opět ACC zapnuty.

### 3. jízda

- Identická jako předchozí

## 11:15 Krátké vyhodnocení v prostoru start/cíl

## 11:20 Jízdy `1+n` (lídr a `n` následovníků) pro postupně zvyšující se `n`

- Asi 10 jízd celkem (jedna jízda cca 5 minut)
- Podle počtu zúčastněných aut rozdělení do dvou skupin (po 4 nebo 5)
- Nejdříve jedna nebo dvě jízdy pro každou skupinu podle scénáře níže.
- Na základě výsledků rozšiřování počtu aut do další jízdy (o dvě v každé jízdě)
- Prvních několik jízd (a možná všechny) pojede lídr rychlostí mezi 50 a 60 km/h (teprve později bude rozhodnuto, zda v několika jízdách i 70-80 km/h).
- Po cestě tam (směrem k apronu) pojede lídr po vjezdu na rovinku rychlostí 50 km/h.
- Zapínání ACC až po výjezdu ze zatáčky, limit 80 km/h, nejvyšší odstup,  
- Až poslední vůz rozpozná ustálení své rychlosti na 50 km/h a vzdálenost k předchozímu vozu neměnnou (asi po několika stech metrech), nahlásí toto vysílačkou lídrovi ("desítka lídrovi, dosáhli jsme ustálené rychlosti"). Možná ale nebudeme provádět, pokud budeme mít nejzazší hranici ustálení odhadnutou z prvních hromadných jízd.
- V reakci na hlášení posledního vozu (pokud bude podáváno) lídr vysílačkou všem ohlásí a následně provede zvýšení rychlosti na 60 km/h ("Lídr všem, zrychluji na 60").
- Pokud dojde k ustálení do úrovně domečku (polovina rovinky), lídr vysílačkou ohlásí a násleně provede snížení z 60 km/h zpět na 50 km/h neagresivním brzděním ("Lídr všem, brzdím na 50").
- Jestli se kvůli neustálené koloně toto nebude moci uskutečnit do úrovně domečku (v polovině dráhy), bude brzdění testováno až v další samostatné jízdě.
- Blízko apronu zahlásí lídr brzdění do apronu a řidiči vypnou adaptivní tempomaty a převezmou řízení.
- Pro vytočení se v apronu se bude bez zastavení pokračovat ve zpáteční cestě. Adaptivní tempomaty zapnuty zase až na rovince. Lídr pojede 60 km/h a opět se bude čekat na ustálení posledního vozu (s hlášením nebo možná bez).
- Po ustálení kolony (asi po několika stech metrech, snad v úrovni domečku) lídr zahlásí a následně provede krátkodobé neagresivní snížení rychlosti z 60 na 50 km/h a asi po 5s zpět na 60 km/h (tzv. rychlostní dip).
- Vypnout tempomaty před odbočováním z rovinky doprava do prostoru start/cíl.
- Po příjezdu do prostoru start/cíl bude provedeno vyhodnocení a ředitel rozhodně, zda se přidají další vozy a zda se změní parametry jízdy.

## 12:30 Zakončení hlavní části experimentu

- Vyhodnocení v domečku
- Prohlédnutí záznamů
- Kdo bude chtít/muset, může už z experimentu odejít

## 13:00 Případně jízdy `1+1` (lídr a `1` následovník), možná se neuskuteční

- Stejný režim jízd jako předchozí sada, ale s jednotlivými vozy, což umožní více opakování skoků v rychlostech během jedné jízdy.
- Více rychlostních rozsahů lídra: 50-60, 70-80, 90-100 km/h.
- Adaptivní tempomaty nastaven vždy o 20 km/h více než lídr.

## 14:00 Zakončení druhé sady experimentálních jízd a celé akce
- vyhodnocení v domečku
- odjezd účastníků

## 15:00 nejzazší čas pro opuštění areálu letiště
