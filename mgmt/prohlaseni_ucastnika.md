# Prohlášení účastníka experimentálního ověřování chování adaptivních tempomatů na letišti Mnichovo Hradiště 6.12.2019

Prohlašuji, že jsem srozuměn s tím, že, ČVUT v Praze dále jen „pořadatel“  ani jeho činovníci, zástupci, či dodavatelé nenesou odpovědnost za žádné škody, stížnosti, soudní řízení a pohledávky v souvislosti se ztrátou, poškozením, ublížením na zdraví  a smrtí, které způsobím svou vlastní nedbalostí nebo úmyslným jednáním při řízení vozidla a v souvislosti s tím v průběhu experimentálního ověřování adaptivních tempomatů dále jen „akce“ na letišti Mnichovo Hradiště dne 6.12.2019.

Potvrzuji, že jsem byl pořadatelem seznámen s plánem akce i poučen o pravidlech bezpečnosti v rámci akce, které jsem účastníkem.

Souhlasím dále s níže uvedenými body a zavazuji se, že je budu respektovat, a toto následně stvrzuji podpisem.

- Jsem si plně vědom toho, že se akce účastním na svoji odpovědnost a pořadatel nenese žádnou zodpovědnost za případné škody na zdraví či majetku, které způsobím svým jednáním.  
- Jsem si vědom veškerých možných důsledků vyplývající z mé účasti na akci, zejména pak ztrát zdravotních, materiálních nebo finančních.
- Účastním se s vozem, jehož technický stav je mi znám a prohlašuji, že je zcela způsobilý k účasti. Jsem plně informován o možnostech ovládání vozu. Zejména pak potvrzuji, že jsem obeznámen s ovládáním adaptivního tempomatu. Nebudu přeceňovat možnosti své ani vozu.
- Znám pravidla bezpečné jízdy a budu je dodržovat. Nebudu přeceňovat své možnosti.
- Prohlašuji, že jsem zdravotně způsobilý se akce zúčastnit a pořadateli jsem nezatajil skutečnosti, které by mohly ohrozit bezpečnost moji nebo ostatních účastníků.
- Před jízdou ani v průběhu jízd nebudu používat žádné látky ovlivňující pozornost, koncentraci nebo vědomí a ohrožující tak schopnost se plně věnovat řízení stroje.




Na Veřejném mezinárodním letišti Mnichovo Hradiště dne 6.12.2018

Jméno a příjmení (hůlkovým písmem):  ....................................................

Podpis: .........................................
