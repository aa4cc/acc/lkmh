# Pravidla bezpečnosti při experimentálním ověřování chování adaptivních tempomatů na letišti Mnichovo Hradiště 6.12.2019

S těmito pravidly bude každý účastník akce seznámen, což potvrdí podpisem v den konání na místě.

## Teminologie (i pro komunikaci vysílačkami)
- **akcí** se rozumí celá pořádaná událost
- **experimentální jízda** či jen **jízda** je část akce, při které se na trati nachází pouze určená vozidla, která realizují experiment. Celá akce se tak skládá z několika jízd a času mezi nimi. Jednou jízdou se rozumí jedno "kolo" po trati, které začíná a končí v místě níže upřesněném jako start/cíl.
- **tratí** se rozumí veškerá zpěvněná plocha v modré bezpečnostní zóně (viz [Příloha č.6 v letištním řádu](http://www.lkmh.cz/wp-content/uploads/2019/06/LKMH-ID-17-02-19-Letistni-rad.pdf), také obrázek níže)
- **start/cíl** je místo v modré zoně na východním konci vzletově-přistávací dráhy (poblíž výjezdu z letiště směrem k závoře), kde se bude začínat i končit každá experimentální jízda.
- **domečkem** se rozumí buňka neziskového spolku [Waypoint, z.s.](http://wptmh.org/) vedle pojížděcí dráhy
- **apron** je místo na západním konci pojezdové dráhy, které ještě patří do modré zony (v mapě apron W, odbavovací plocha). Bude sloužit jako točna v polovině jízdy.
- **ředitel** akce je hlavní pořadatel (Zdeněk Hurák, ČVUT)
- **lídr** je určené auto, které ve všech experimentálních jízdách jezdí jako první a svou rychlostí definuje rychlost aut, která jedou za ním.
- Každému autu s adaptivním tempomatem bude před zahájením experimentu přiřazeno číslo (od 1 do 10) a podle něj bude auto označováno jako **jednička**, **dvojka** až **desítka**.

![](zones.png)

## Pravidla

### Chování na letišti

- Každý účastník bude pořadatelem či jím pověřeným pracovníkem proškolen o pravidlech pohybu v prostoru letiště. Zejména bude znát hranice modré zóny, která jediná je pro účastníky akce povolená.

### Organizace akce, řízení experimentu

- Akce je řízena jejím ředitelem. Vyšší prioritu mají ale rozhodnutí služby řízení provozu letiště.
- Veškerý pohyb vozů po trati mimo dobu konání organizované experimentální jízdy, jako jsou příjezd a odjezd na/z letiště, bude v rychlosti omezen na 50 km/h. Při takovém pohybu musí mít vozy zapnuta výstražná světla.
- Zahájení, přerušení a ukončení experimentální jízdy je vyhlášeno ředitelem akce vysílačkou. Během experimentu se auta mohou po dráze pohybovat jedině za vedoucím vozidlem. Žádné jiné auto nesmí být v době experimentu být na trati. Účastníci, kteří zrovna nejsou zapojeni do experimentální jízdy se zdržují v domečku nebo jeho bezprostředním okolí v dostatečné vzdálenosti od trati.
- V průběhu experimentální jízdy se nesmí žádné auto vyskytnout na trati samostatně bez lídra. V případě, že z technických důvodů jeden z vozů nedokončí jízdu, musí být experiment přerušen a nesmí se konat další experimentální jízda, dokud není vůz mimo trať na místě podle pokynů ředitele.

### Vůz, účastník (řidič)

- Akce se mohou účastnit jen dopředu nahlášení a pořadatelem schválení účastníci
- Účastnit se mohou jen vozy způsobilé provozu po veřejných komunikacích (platná STK)
- Každý vůz musí mít zimní pneumatiky.
- Majité vozů podpisem prohlášení potvrdí, že si nejsou vědomi závady na technického stavu vozu a že jsou obeznámeni s technickými možnostmi i omezeními vozu, jak je udává výrobce (zejména adaptivního tempomatu).
- Stejným podpisem majitelé vozu potvrdí, že jejich zdravotní stav jim nebrání v účasti na experimentu. Účastníci nesmí požít látky omezující pozornost a koncentraci.
- Po celou dobu experimentu nesmí mít vůz vypnuty žádné své bezpečnostní prvky (ABS, ESP, ...)

### Spolujezdci, vysílačky

- Během experimentu musí být v každém vozu na trati kromě řidiče i přidělený spolujezdec.
- Během experimentu musí být v každém vozu vysílačka nastavená na zadanou frekvenci a zapnutá na příjem. Obsluhuje ji spolujezdec. Je zakázáno frekvenci měnit.
- Řidič i spolujezdec musí být během jízdy připoutáni bezpečnostními pásy.

### Zahajování a ukončování experimentálních jízd
- Každá experimentální jízda bude zahajována z klidu v prostoru start/cíl a ve stejném místě bude úplným zastavením ukončována.
- Před jízdou budou v prostoru start/cíl stručně vysvětleny parametry jízdy: rozsah hodnot rychlostí (např. "pojedeme v rozsahu 50-60 km/h, adaptivní tempomaty nastavit na 80 km/h, nastavit největší časový odstup, po cestě tam nejdříve zrychlení z 50 na 60 km/h, pak ustálení a snížení rychlosti z 60 na 50 km/h, ustálení, dojezd do apronu. Po cestě zpět 60 km/h, pětisekundové snížení na 50 km/h a zase zvýšení na 60 km/h).
- Experimentální jízda nebude ředitelem spuštěna, pokud nebude mít ředitel informaci od všech aut, kde se nachází
- Před experimentální jízdou potvrzení (vysílačkou) od všech aut, že jsou připraveny ("lídr připraven", "jednička připravena", ..., "desítka připravena"). Teprve pak ředitel dá pokyn k zahájení jízdy.
- Pokud bude mít libovolné auto potřebu zastavit v průběhu experimentální jízdy, hlásí to vysílačkou všem ("trojka všem, musíme zastavit"). Poté plynule zastavují všichni a experimentální jízda končí. Po zastavení a vyřešení problému se všechny auta rozjedou na pokyn ředitele, ale už nepokračují v plnění experimentu, nýbrž se vrací na start/cíl a dále podle upřesnění ředitelem.
- Bezprostředně po dokončení jízdy a před začátkem další či přesunem do domečku bude hned v prostoru start/cíl ředitelem provedeno vyhodnocení jízdy (obejde vozy a zkonzultuje naměřená data u spolujezdců).

### Průběh experimentální jízdy

- Lídr je vždy první. Za žádných okolností nesmí další auta předjet lídra.
- Každý vůz bude na své zadní části označen číslem (lepicí páskou přilepený papír s vytištěným číslem). Své vlastní číslo bude mít řidič nalepeno i na palubní desce.
- Pořadí ostatních vozů bude určeno ředitelem a v průběhu jedné jízdy se nebude měnit.
- Podle přiděleného pořadového čísla bude mít každé auto přidělen i směr (doleva nebo doprava) pro případ nouzového brzdění. Lichá čísla doleva (do volného pruhu či v mezním případě na trávu), sudá čísla doprava (ke krajnici či v mezním případě na trávu). Při případné změně pořadí aut ředitelem musí být mezi auty prohozena i čísla (výměny preferovány takové, které zachovají směr šipek).
- I přestože v průběhu celého experimentu se nesetkají auta v protisměru, bude se v experimentálních jízdách jezdit zásadně v pravém pruhu. Závěrem trati (apronem) se tedy projíždí proti směru hodinových ručiček.
- Žádné podjíždění v zatáčkách (po výjezdu ze startu/cíle do dvojité levotočivé zatáčky a pak na apronu)
- Zatáčky musí být projížděny plynule (žádný smyk ani nic podobného).
- Adaptivní tempomat musí být vypnut při průjezdu zatáčkami na obou koncích pojezdové dráhy. Zapnuty budou teprve pro vjezdu na rovinku.
- V průběhu jízdy bude před začátkem zrychlování či brzdění toto lídrem hlášeno všem vozům vysílačkou ("Lídr všem: brzdíme na 50 km/h" nebo "Lídr všem, brzdím na 50 km/h a hned zase zrychlím na 60 km/h").
- Řidiči musí i při zapnutém adaptivním tempomatu neustále věnovat pozornost řízení (nohu na brzdovém pedálu, držet volant oběma rukama) a být připraveni převzít řízení.
- Spolujezdec sleduje, zda se při přechodu na zadanou rychlost dochází k překmitu či podkmitu vlastní rychlosti (například při ohlášeném brzdění lídra z 60 na 50 km/h se bude rychlost vlastního vozu dostávat viditelně pod 50 km/h).

### Zdravotnický dozor

- Na akci bude po celou dobu jejího konání přítomen zdravotnický dozor (hasič/záchranář)
