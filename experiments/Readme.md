Notes
=======================

Experiment 1 (start 11:49)
------------------------------

- Car 7 - Data not recorded
- Car 9 - Data not present (not recorded?)

- Car 6 - Need merge (2 files)
- CAR 4 - IMU missing

Experiment 2 (start 11:54)
------------------------------

- Car 7 - Data not recorded

- Car 2 - IMU missing
- Car 4 - IMU missing 

Experiment 3 (start 11:59)
------------------------------

- Car 7 - Data not recorded

- Car 4 - IMU missing

Experiment 4 (start 12:08)
------------------------------

- Car 7 - Data not recorded

- Car 4 - IMU missing

Experiment 5 (start 12:15)
------------------------------
Only leader and cars 1-4
- Car 4 - IMU missing

Experiment 6 (start 12:25)
------------------------------
Only leader and cars 5-8
- Car 7 - Data not recorded

Experiment 7 (start 12:33)
------------------------------
Only leader and cars 9-11

Experiment 8 (start 12:42)
------------------------------
- Car 4 - IMU missing
- Car 7 - Data not recorded

Experiment 9 (start 12:54)
------------------------------
- Car 4 - IMU missing
- Car 6 - IMU missing
- Car 7 - Data not recorded

Experiment 10 (start 13:03)
------------------------------
- Car 4 - IMU missing
- Car 6 - IMU missing
- Car 7 - Data not recorded

- Car 11 - Merged

Experiment 11 (start 13:10)
------------------------------
- Car 4 - IMU missing
- Car 6 - IMU missing

Experiment 12 (start 14:10)
------------------------------
Only leader and cars 1,3,4,5,7,8,10

- Car 4 - IMU missing