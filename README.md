# ACC@LKMH

## CZ
Materiály (texty, data, grafy, odkazy) k jednodennímu experimentu s kolonou 12 běžných osobních vozů vybavených adaptivními tempomaty konanému za zapojení veřejnosti na Letišti Mnichovo Hradiště (LKMH) v pátek 6.prosince 2019.

## EN
Materials (texts, data, plots, links) for a one-day experiment with a platoon of 12 commercial cars with adaptive cruise control conducted at International Public Airport Mnichovo Hradiště on Friday, December 6, 2019.
